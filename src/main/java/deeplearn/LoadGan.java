package deeplearn;

public class LoadGan {
 
	/*public static void main(String[] args) throws Exception {
	    ComputationGraph restored = ComputationGraph.load(new File(Gan.model), true);
		
		DataSetIterator train = new MnistDataSetIterator(30, true, 12345);
		INDArray trueExp = train.next().getFeatures();
		Map<String, INDArray> map = restored.feedForward(
				new INDArray[] { Nd4j.rand(new NormalDistribution(),new long[] { 50, 10 }), trueExp }, false);
		INDArray indArray = map.get("g3");// .reshape(20,28,28);
		List<INDArray> list = new ArrayList<>();
		for (int j = 0; j < indArray.size(0); j++) {
			list.add(indArray.getRow(j));
		}
	    
		MNISTVisualizer bestVisualizer = new MNISTVisualizer(1, list, "Gan");
 
		bestVisualizer.visualize();
	}
	
	
	public static class MNISTVisualizer {
		private double imageScale;
		private List<INDArray> digits; // Digits (as row vectors), one per
										// INDArray
		private String title;
		private int gridWidth;
 
		public MNISTVisualizer(double imageScale, List<INDArray> digits, String title) {
			this(imageScale, digits, title, 5);
		}
 
		public MNISTVisualizer(double imageScale, List<INDArray> digits, String title, int gridWidth) {
			this.imageScale = imageScale;
			this.digits = digits;
			this.title = title;
			this.gridWidth = gridWidth;
		}
 
		public void visualize() {
			JFrame frame = new JFrame();
			frame.setTitle(title);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 
			JPanel panel = new JPanel();
			panel.setLayout(new GridLayout(0, gridWidth));
 
			List<JLabel> list = getComponents();
			for (JLabel image : list) {
				panel.add(image);
			}
 
			frame.add(panel);
			frame.setVisible(true);
			frame.pack();
		}
 
		public List<JLabel> getComponents() {
			List<JLabel> images = new ArrayList<>();
			for (INDArray arr : digits) {
				BufferedImage bi = new BufferedImage(28, 28, BufferedImage.TYPE_BYTE_GRAY);
				for (int i = 0; i < 784; i++) {
					bi.getRaster().setSample(i % 28, i / 28, 0, (int) (255 * arr.getDouble(i)));
				}
				ImageIcon orig = new ImageIcon(bi);
				Image imageScaled = orig.getImage().getScaledInstance((int) (imageScale * 28), (int) (imageScale * 28),
						Image.SCALE_DEFAULT);
				ImageIcon scaled = new ImageIcon(imageScaled);
				images.add(new JLabel(scaled));
			}
			return images;
		}
	}*/
}
