# first-order-model-java

#### 介绍

让静态图片动起来，这是first-order-model最让人兴奋的地方

1. 目前网上全部是以python语言开发的，java程序员想要研究和使用还是有些难度的。
2. 本项目得益于DJL，可以让java运行AI模型，DJL是一个java的deeplean工具，可以调用TensorFlow,pyTorch,mxnet等等AI引擎。java程序员可以入坑。 

#### 模型下载地址
https://pan.baidu.com/s/1veqKtJiHpIBk4eMIv9Zn0Q   密码： 1234 

1、运行后效果图(视频内容只是技术研究使用，如果侵犯，请通知删除，谢谢)


![源图](https://gitee.com/endlesshh/first-order-model-java/raw/master/img/2m.jpg)
![源视频](https://gitee.com/endlesshh/first-order-model-java/raw/master/img/ds.gif)
![图片效果](https://gitee.com/endlesshh/first-order-model-java/raw/master/img/file.gif)

 Required Software
------------
本版本在以下平台测试通过：
* windows7 64bit
* jdk1.8.0_45
* DJL 0.11.0
* opencv4.3
* javaccp1.5.3 

#### 使用源码生成模型
1、python源码模型名称vox-adv-cpk.pth.tar在百度网盘中
2、git clone https://gitee.com/endlesshh/fisrt-order-model
3、按要求安装相应的包
4、demo.py  --config config/vox-256.yaml  --driving_video data/ds1.mp4 --source_image sup-mat/3m.jpg  --checkpoint  模型路径/vox-adv-cpk.pth.tar --relative --adapt_scale
5、demo.py 在65 和85行去掉注释，保存模型

#### 源项目地址
1. https://github.com/AliaksandrSiarohin/first-order-model(源项目)
2. https://gitee.com/endlesshh/fisrt-order-model(为了适配DJL修改了模型参数,以当前项目导出的模型)
